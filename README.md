# Lincoln Avenue Corridor Study Input Map - Scenarios Phase

Interactive input map for the [Placeholder](https://lrtp.cuuats.org/) using [Web Map
GL](https://gitlab.com/ccrpc/webmapgl).

## Building

This project is hosted via gitlab pages, the CI/CD is set up to build those pages whenever something is pushed to either the main branch or the create-pages branch.

## Scenario Rasters

Planners provided the three different scenarios as geo referenced .tiff files.

To convert these to mbitles we modified the steps on the wiki for [GIS Imagery Conversion](https://wiki.ccrpc.org/pcd/gis-imagery-conversion)

I usually run these commands in a folder called `~/temp` but you should be able to run them anywhere.

The commands for each scenario are:

```bash
cp "/mnt/GIS/CUUATS/Lincoln Avenue Corridor Study/Data/GEOREFERENCED Scenario Visualization 1.tif" .

gdal_translate --config GDAL_CACHEMAX 100% -of MBTILES \
  -co ZOOM_LEVEL_STRATEGY="LOWER" -co NAME="Lincoln Avenue Scenario 1" \
  -co DESCRIPTION="Scenario 1 for a potential change to the Lincoln Avenue Corridor" \
  -co TYPE="baselayer" -co TILE_FORMAT=PNG \
  "GEOREFERENCED Scenario Visualization 1.tif" "lincoln-avenue-scenario-1.mbtiles"

gdaladdo --config GDAL_CACHEMAX 100% \
  --config PHOTOMETRIC_OVERVIEW YCBCR --config PNG_QUALITY_OVERVIEW 100 \
  -r cubic lincoln-avenue-scenario-1.mbtiles

cp lincoln-avenue-scenario-1.mbtiles /mnt/GIS/Resources/Webmaps/lincoln-avenue-scenario-tiles/

rm "GEOREFERENCED Scenario Visualization 1.tif" lincoln-avenue-scenario-1.mbtiles

```

```bash
cp "/mnt/GIS/CUUATS/Lincoln Avenue Corridor Study/Data/GEOREFERENCED Scenario Visualization 2.tif" .

gdal_translate --config GDAL_CACHEMAX 100% -of MBTILES \
  -co ZOOM_LEVEL_STRATEGY="LOWER" -co NAME="Lincoln Avenue Scenario 2" \
  -co DESCRIPTION="Scenario 2 for a potential change to the Lincoln Avenue Corridor" \
  -co TYPE="baselayer" -co TILE_FORMAT=PNG \
  "GEOREFERENCED Scenario Visualization 2.tif" "lincoln-avenue-scenario-2.mbtiles"

gdaladdo --config GDAL_CACHEMAX 100% \
  --config PHOTOMETRIC_OVERVIEW YCBCR --config PNG_QUALITY_OVERVIEW 100 \
  -r cubic lincoln-avenue-scenario-2.mbtiles

cp lincoln-avenue-scenario-2.mbtiles /mnt/GIS/Resources/Webmaps/lincoln-avenue-scenario-tiles/

rm "GEOREFERENCED Scenario Visualization 2.tif" lincoln-avenue-scenario-2.mbtiles

```

```bash
cp "/mnt/GIS/CUUATS/Lincoln Avenue Corridor Study/Data/GEOREFERENCED Scenario Visualization 3.tif" .

gdal_translate --config GDAL_CACHEMAX 100% -of MBTILES \
  -co ZOOM_LEVEL_STRATEGY="LOWER" -co NAME="Lincoln Avenue Scenario 3" \
  -co DESCRIPTION="Scenario 3 for a potential change to the Lincoln Avenue Corridor" \
  -co TYPE="baselayer" -co TILE_FORMAT=PNG \
  "GEOREFERENCED Scenario Visualization 3.tif" "lincoln-avenue-scenario-3.mbtiles"

gdaladdo --config GDAL_CACHEMAX 100% \
  --config PHOTOMETRIC_OVERVIEW YCBCR --config PNG_QUALITY_OVERVIEW 100 \
  -r cubic lincoln-avenue-scenario-3.mbtiles

cp lincoln-avenue-scenario-3.mbtiles /mnt/GIS/Resources/Webmaps/lincoln-avenue-scenario-tiles/

rm "GEOREFERENCED Scenario Visualization 3.tif" lincoln-avenue-scenario-3.mbtiles

```

We placed the resulting mbtile files in the G:/Resources/Webmaps/lincoln-avenue-scenario-tiles folder on the remote drive so that maps.ccrpc.org would serve them.

## Creating Tables in PCD Database

Scenario One

```sql
-- SEQUENCE: lincoln_avenue_scenarios.scenario_one_comment_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_one_comment_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_one_comment_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_one_comment_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_one_comment_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_one_comment_id TO web_public;

-- Table: lincoln_avenue_scenarios.scenario_one_comment

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_one_comment;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_one_comment
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_one_comment_id'::regclass),
    geom geometry(Point,3435),
    comment_type character varying(100) COLLATE pg_catalog."default",
    comment_description text COLLATE pg_catalog."default",
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    _moderation character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT scenario_one_comments_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_one_comment
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_one_comment FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_one_comment TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_one_comment TO web_public;


-- FUNCTION: lincoln_avenue_scenarios.comment_auto_approve()

-- DROP FUNCTION IF EXISTS lincoln_avenue_scenarios.comment_auto_approve();

CREATE OR REPLACE FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
        IF NEW.comment_description IS NULL OR NEW.comment_description = '' THEN
            NEW._moderation := 'approved';
        END IF;
        RETURN NEW;
    END;
$BODY$;

ALTER FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    OWNER TO pcd_admin;


-- Trigger: comment_auto_approve

-- DROP TRIGGER IF EXISTS comment_auto_approve ON lincoln_avenue_scenarios.scenario_one_comment;

CREATE TRIGGER comment_auto_approve
    BEFORE INSERT
    ON lincoln_avenue_scenarios.scenario_one_comment
    FOR EACH ROW
    EXECUTE FUNCTION lincoln_avenue_scenarios.comment_auto_approve();


-- Likes --

-- SEQUENCE: lincoln_avenue_scenarios.scenario_one_like_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_one_like_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_one_like_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_one_like_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_one_like_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_one_like_id TO web_public;


-- Table: lincoln_avenue_scenarios.scenario_one_comment_like

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_one_comment_like;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_one_comment_like
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_one_like_id'::regclass),
    feature_id integer NOT NULL,
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    action character varying(10) COLLATE pg_catalog."default",
    client_id character varying(64) COLLATE pg_catalog."default",
    CONSTRAINT scenario_one_like_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_one_comment_like
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_one_comment_like FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_one_comment_like TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_one_comment_like TO web_public;


-- View: lincoln_avenue_scenarios.scenario_one_comment_web

-- DROP VIEW lincoln_avenue_scenarios.scenario_one_comment_web;

CREATE OR REPLACE VIEW lincoln_avenue_scenarios.scenario_one_comment_web
 AS
 SELECT cm.id,
    cm.comment_type,
    cm.comment_description,
    lower("substring"(cm.comment_type::text, '^([\w\-]+)'::text)) AS comment_mode,
    cm._created,
    COALESCE(lk.likes, 0::numeric)::integer AS _likes,
    cm.geom
   FROM lincoln_avenue_scenarios.scenario_one_comment cm
     LEFT JOIN ( SELECT feature_clients.feature_id,
            sum(feature_clients.liked) AS likes
           FROM ( SELECT scenario_one_comment_like.feature_id,
                    scenario_one_comment_like.client_id,
                    LEAST(GREATEST(sum(
                        CASE
                            WHEN scenario_one_comment_like.action::text = 'like'::text THEN 1
                            ELSE '-1'::integer
                        END), 0::bigint), 1::bigint) AS liked
                   FROM lincoln_avenue_scenarios.scenario_one_comment_like
                  GROUP BY scenario_one_comment_like.feature_id, scenario_one_comment_like.client_id) feature_clients
          GROUP BY feature_clients.feature_id) lk ON cm.id = lk.feature_id
  WHERE cm._moderation::text = 'approved'::text AND cm.comment_type IS NOT NULL;

ALTER TABLE lincoln_avenue_scenarios.scenario_one_comment_web
    OWNER TO pcd_admin;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_one_comment_web TO pcd_admin;
GRANT SELECT ON TABLE lincoln_avenue_scenarios.scenario_one_comment_web TO web_public;
```

Scenario Two:

```sql
-- SEQUENCE: lincoln_avenue_scenarios.scenario_two_comment_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_two_comment_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_two_comment_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_two_comment_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_two_comment_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_two_comment_id TO web_public;

-- Table: lincoln_avenue_scenarios.scenario_two_comment

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_two_comment;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_two_comment
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_two_comment_id'::regclass),
    geom geometry(Point,3435),
    comment_type character varying(100) COLLATE pg_catalog."default",
    comment_description text COLLATE pg_catalog."default",
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    _moderation character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT scenario_two_comments_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_two_comment
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_two_comment FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_two_comment TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_two_comment TO web_public;


-- FUNCTION: lincoln_avenue_scenarios.comment_auto_approve()

-- DROP FUNCTION IF EXISTS lincoln_avenue_scenarios.comment_auto_approve();

CREATE OR REPLACE FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
        IF NEW.comment_description IS NULL OR NEW.comment_description = '' THEN
            NEW._moderation := 'approved';
        END IF;
        RETURN NEW;
    END;
$BODY$;

ALTER FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    OWNER TO pcd_admin;


-- Trigger: comment_auto_approve

-- DROP TRIGGER IF EXISTS comment_auto_approve ON lincoln_avenue_scenarios.scenario_two_comment;

CREATE TRIGGER comment_auto_approve
    BEFORE INSERT
    ON lincoln_avenue_scenarios.scenario_two_comment
    FOR EACH ROW
    EXECUTE FUNCTION lincoln_avenue_scenarios.comment_auto_approve();


-- Likes --

-- SEQUENCE: lincoln_avenue_scenarios.scenario_two_like_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_two_like_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_two_like_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_two_like_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_two_like_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_two_like_id TO web_public;


-- Table: lincoln_avenue_scenarios.scenario_two_comment_like

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_two_comment_like;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_two_comment_like
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_two_like_id'::regclass),
    feature_id integer NOT NULL,
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    action character varying(10) COLLATE pg_catalog."default",
    client_id character varying(64) COLLATE pg_catalog."default",
    CONSTRAINT scenario_two_like_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_two_comment_like
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_two_comment_like FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_two_comment_like TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_two_comment_like TO web_public;


-- View: lincoln_avenue_scenarios.scenario_two_comment_web

-- DROP VIEW lincoln_avenue_scenarios.scenario_two_comment_web;

CREATE OR REPLACE VIEW lincoln_avenue_scenarios.scenario_two_comment_web
 AS
 SELECT cm.id,
    cm.comment_type,
    cm.comment_description,
    lower("substring"(cm.comment_type::text, '^([\w\-]+)'::text)) AS comment_mode,
    cm._created,
    COALESCE(lk.likes, 0::numeric)::integer AS _likes,
    cm.geom
   FROM lincoln_avenue_scenarios.scenario_two_comment cm
     LEFT JOIN ( SELECT feature_clients.feature_id,
            sum(feature_clients.liked) AS likes
           FROM ( SELECT scenario_two_comment_like.feature_id,
                    scenario_two_comment_like.client_id,
                    LEAST(GREATEST(sum(
                        CASE
                            WHEN scenario_two_comment_like.action::text = 'like'::text THEN 1
                            ELSE '-1'::integer
                        END), 0::bigint), 1::bigint) AS liked
                   FROM lincoln_avenue_scenarios.scenario_two_comment_like
                  GROUP BY scenario_two_comment_like.feature_id, scenario_two_comment_like.client_id) feature_clients
          GROUP BY feature_clients.feature_id) lk ON cm.id = lk.feature_id
  WHERE cm._moderation::text = 'approved'::text AND cm.comment_type IS NOT NULL;

ALTER TABLE lincoln_avenue_scenarios.scenario_two_comment_web
    OWNER TO pcd_admin;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_two_comment_web TO pcd_admin;
GRANT SELECT ON TABLE lincoln_avenue_scenarios.scenario_two_comment_web TO web_public;
```

Scenario Three:

```sql
-- SEQUENCE: lincoln_avenue_scenarios.scenario_three_comment_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_three_comment_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_three_comment_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_three_comment_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_three_comment_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_three_comment_id TO web_public;

-- Table: lincoln_avenue_scenarios.scenario_three_comment

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_three_comment;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_three_comment
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_three_comment_id'::regclass),
    geom geometry(Point,3435),
    comment_type character varying(100) COLLATE pg_catalog."default",
    comment_description text COLLATE pg_catalog."default",
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    _moderation character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT scenario_three_comments_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_three_comment
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_three_comment FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_three_comment TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_three_comment TO web_public;


-- FUNCTION: lincoln_avenue_scenarios.comment_auto_approve()

-- DROP FUNCTION IF EXISTS lincoln_avenue_scenarios.comment_auto_approve();

CREATE OR REPLACE FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
        IF NEW.comment_description IS NULL OR NEW.comment_description = '' THEN
            NEW._moderation := 'approved';
        END IF;
        RETURN NEW;
    END;
$BODY$;

ALTER FUNCTION lincoln_avenue_scenarios.comment_auto_approve()
    OWNER TO pcd_admin;


-- Trigger: comment_auto_approve

-- DROP TRIGGER IF EXISTS comment_auto_approve ON lincoln_avenue_scenarios.scenario_three_comment;

CREATE TRIGGER comment_auto_approve
    BEFORE INSERT
    ON lincoln_avenue_scenarios.scenario_three_comment
    FOR EACH ROW
    EXECUTE FUNCTION lincoln_avenue_scenarios.comment_auto_approve();


-- Likes --

-- SEQUENCE: lincoln_avenue_scenarios.scenario_three_like_id

-- DROP SEQUENCE IF EXISTS lincoln_avenue_scenarios.scenario_three_like_id;

CREATE SEQUENCE IF NOT EXISTS lincoln_avenue_scenarios.scenario_three_like_id
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE lincoln_avenue_scenarios.scenario_three_like_id
    OWNER TO pcd_admin;

GRANT ALL ON SEQUENCE lincoln_avenue_scenarios.scenario_three_like_id TO pcd_admin;

GRANT SELECT, USAGE ON SEQUENCE lincoln_avenue_scenarios.scenario_three_like_id TO web_public;


-- Table: lincoln_avenue_scenarios.scenario_three_comment_like

-- DROP TABLE IF EXISTS lincoln_avenue_scenarios.scenario_three_comment_like;

CREATE TABLE IF NOT EXISTS lincoln_avenue_scenarios.scenario_three_comment_like
(
    id integer NOT NULL DEFAULT nextval('lincoln_avenue_scenarios.scenario_three_like_id'::regclass),
    feature_id integer NOT NULL,
    _ip character varying(45) COLLATE pg_catalog."default",
    _created timestamp without time zone,
    _modified timestamp without time zone,
    action character varying(10) COLLATE pg_catalog."default",
    client_id character varying(64) COLLATE pg_catalog."default",
    CONSTRAINT scenario_three_like_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS lincoln_avenue_scenarios.scenario_three_comment_like
    OWNER to pcd_admin;

REVOKE ALL ON TABLE lincoln_avenue_scenarios.scenario_three_comment_like FROM web_public;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_three_comment_like TO pcd_admin;

GRANT INSERT, SELECT ON TABLE lincoln_avenue_scenarios.scenario_three_comment_like TO web_public;


-- View: lincoln_avenue_scenarios.scenario_three_comment_web

-- DROP VIEW lincoln_avenue_scenarios.scenario_three_comment_web;

CREATE OR REPLACE VIEW lincoln_avenue_scenarios.scenario_three_comment_web
 AS
 SELECT cm.id,
    cm.comment_type,
    cm.comment_description,
    lower("substring"(cm.comment_type::text, '^([\w\-]+)'::text)) AS comment_mode,
    cm._created,
    COALESCE(lk.likes, 0::numeric)::integer AS _likes,
    cm.geom
   FROM lincoln_avenue_scenarios.scenario_three_comment cm
     LEFT JOIN ( SELECT feature_clients.feature_id,
            sum(feature_clients.liked) AS likes
           FROM ( SELECT scenario_three_comment_like.feature_id,
                    scenario_three_comment_like.client_id,
                    LEAST(GREATEST(sum(
                        CASE
                            WHEN scenario_three_comment_like.action::text = 'like'::text THEN 1
                            ELSE '-1'::integer
                        END), 0::bigint), 1::bigint) AS liked
                   FROM lincoln_avenue_scenarios.scenario_three_comment_like
                  GROUP BY scenario_three_comment_like.feature_id, scenario_three_comment_like.client_id) feature_clients
          GROUP BY feature_clients.feature_id) lk ON cm.id = lk.feature_id
  WHERE cm._moderation::text = 'approved'::text AND cm.comment_type IS NOT NULL;

ALTER TABLE lincoln_avenue_scenarios.scenario_three_comment_web
    OWNER TO pcd_admin;

GRANT ALL ON TABLE lincoln_avenue_scenarios.scenario_three_comment_web TO pcd_admin;
GRANT SELECT ON TABLE lincoln_avenue_scenarios.scenario_three_comment_web TO web_public;
```

## License

LRTP Input Map is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/lincoln-avenue-scenario-phase/blob/main/LICENSE.md).
