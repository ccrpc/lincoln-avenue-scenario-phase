import flatdict
import json
import csv
import os

with open(os.getcwd() + "/" + "en.json") as file:
    languageConfig = json.load(file)

flattened = flatdict.FlatDict(languageConfig, delimiter=".")


with open(os.getcwd() + "/" + "en.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(["value", "English Phrase"])
    writer.writerows(flattened.items())
