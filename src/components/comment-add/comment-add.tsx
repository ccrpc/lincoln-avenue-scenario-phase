import { h, Component, Prop, State } from "@stencil/core";
import { _t } from "../i18n/i18n";
import { findOrCreateOnReady } from "../utils";
import { toastController } from "@ionic/core";

@Component({
  tag: "lrtp-comment-add",
})
export class CommentAdd {
  @State() visible: boolean = true;

  private drawCtrl!: HTMLGlDrawControllerElement;
  private formCtrl!: HTMLGlFormControllerElement;
  private restCtrl!: HTMLGlRestControllerElement;

  /**
   * The label for the gl-form used to create the comment.
   */
  @Prop() readonly label: string;

  /**
   * Url to the schema used by the gl-form for adding a comment
   */
  @Prop() readonly schema: string;

  /**
   * Authentication token for the Features API
   */
  @Prop() readonly token: string;

  /**
   * The label of the draw toolbar used for selecting where a comment is.
   */
  @Prop() readonly toolbarLabel: string;

  /**
   * The Features API url for adding new comments
   */
  @Prop() readonly url: string;

  private async start() {
    this.visible = false;

    if (this.drawCtrl == undefined)
      this.drawCtrl = await findOrCreateOnReady("gl-draw-controller");

    let fc = await this.drawCtrl.create(null, {
      toolbarLabel: this.toolbarLabel,
    });

    if (!fc || !fc["features"].length) return this.end();

    if (this.formCtrl == undefined)
      this.formCtrl = await findOrCreateOnReady("gl-form-controller");

    const feature: { properties: { [key: string]: any } } =
      await this.formCtrl.create(fc["features"][0], {
        label: this.label,
        schema: this.schema,
        translateText: true,
      });

    if (feature == undefined) return this.end();

    let ok = false;
    try {
      if (this.restCtrl == undefined)
        this.restCtrl = await findOrCreateOnReady("gl-rest-controller");

      let res = await this.restCtrl.create(feature, {
        url: this.url,
        token: this.token,
      });
      ok = res.ok;
    } finally {
      this.showToast(ok, feature);
      this.end();
    }
  }

  private end() {
    this.visible = true;
  }

  private async showToast(
    success: boolean,
    feature?: { properties: { [key: string]: any } }
  ) {
    let message: string;
    if (success) {
      let desc = feature.properties.comment_description;
      message =
        desc == undefined || desc === ""
          ? _t("lrtp.app.comment.added")
          : _t("lrtp.app.comment.moderation");
    } else {
      message = _t("lrtp.app.comment.error");
    }

    let options = {
      message: message,
      duration: 3000,
    };

    let toast = await toastController.create(options);
    await toast.present();
    return toast;
  }

  private doStartDraw = () => this.start();

  render() {
    if (this.visible)
      return (
        <ion-fab-button onClick={this.doStartDraw}>
          <ion-icon name="add"></ion-icon>
        </ion-fab-button>
      );
  }
}
