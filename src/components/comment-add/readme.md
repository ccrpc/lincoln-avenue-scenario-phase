# lrtp-comment-add



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                          | Type     | Default     |
| -------------- | --------------- | -------------------------------------------------------------------- | -------- | ----------- |
| `label`        | `label`         | The label for the gl-form used to create the comment.                | `string` | `undefined` |
| `schema`       | `schema`        | Url to the schema used by the gl-form for adding a comment           | `string` | `undefined` |
| `token`        | `token`         | Authentication token for the Features API                            | `string` | `undefined` |
| `toolbarLabel` | `toolbar-label` | The label of the draw toolbar used for selecting where a comment is. | `string` | `undefined` |
| `url`          | `url`           | The Features API url for adding new comments                         | `string` | `undefined` |


## Dependencies

### Used by

 - [lrtp-app](../app)

### Depends on

- ion-fab-button
- ion-icon

### Graph
```mermaid
graph TD;
  lrtp-comment-add --> ion-fab-button
  lrtp-comment-add --> ion-icon
  ion-fab-button --> ion-icon
  ion-fab-button --> ion-ripple-effect
  lrtp-app --> lrtp-comment-add
  style lrtp-comment-add fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
