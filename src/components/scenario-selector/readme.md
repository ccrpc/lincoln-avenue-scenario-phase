# lrtp-scenario-selector



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                  | Type                  | Default     |
| --------------- | ---------------- | -------------------------------------------- | --------------------- | ----------- |
| `scenarioIndex` | `scenario-index` | The index of the currently selected scenario | `number`              | `0`         |
| `scenarios`     | --               | The scenarios the user can select from       | `scenarioInterface[]` | `undefined` |


## Events

| Event              | Description                                | Type                  |
| ------------------ | ------------------------------------------ | --------------------- |
| `scenarioSelected` | Event for when the user selects a scenario | `CustomEvent<number>` |


## Dependencies

### Used by

 - [lrtp-app](../app)

### Depends on

- ion-select
- ion-select-option

### Graph
```mermaid
graph TD;
  lrtp-scenario-selector --> ion-select
  lrtp-scenario-selector --> ion-select-option
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-label
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  lrtp-app --> lrtp-scenario-selector
  style lrtp-scenario-selector fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
