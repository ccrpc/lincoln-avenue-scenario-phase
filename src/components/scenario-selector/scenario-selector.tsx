import { Component, Host, Prop, Event, EventEmitter, h } from "@stencil/core";
import { scenarioInterface } from "../../public/scenarios";
import { SelectCustomEvent } from "@ionic/core";
import { _t } from "../i18n/i18n";

@Component({
  tag: "lrtp-scenario-selector",
  styleUrl: "scenario-selector.css",
  shadow: true,
})
export class ScenarioSelector {
  /**
   * The scenarios the user can select from
   */
  @Prop() readonly scenarios: scenarioInterface[];

  /**
   * The index of the currently selected scenario
   */
  @Prop() readonly scenarioIndex: number = 0;

  /**
   * Event for when the user selects a scenario
   */
  @Event() scenarioSelected: EventEmitter<number>;

  /**
   * When the user selects a scenario, the index of that scenario is emitted
   */
  private handleSelectionChange = (event: SelectCustomEvent<string>) => {
    this.scenarioSelected.emit(
      this.scenarios
        .map((scenario) => scenario.label)
        .indexOf(event.detail.value)
    );
  };

  render() {
    return (
      <Host>
        <ion-select
          placeholder={_t("lrtp.app.scenarios.selection.text")}
          interface="popover"
          onIonChange={this.handleSelectionChange}
          value={this.scenarios[this.scenarioIndex].label}
          label={_t("lrtp.app.scenarios.label")}
        >
          {this.scenarios.map((scenario: scenarioInterface) => (
            <ion-select-option value={scenario.label}>
              {_t(scenario.description)}
            </ion-select-option>
          ))}
        </ion-select>
      </Host>
    );
  }
}
