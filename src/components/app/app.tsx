import {
  h,
  Component,
  Element,
  Listen,
  Prop,
  State,
  Host,
} from "@stencil/core";
import { findOrCreateOnReady } from "../utils";
import { Driver, driver as createDriver } from "driver.js";
import { _t } from "../i18n/i18n";
import { Feature, Point } from "geojson";
import { commentsStyle as _commentsStyle } from "../../public/styles/commentsStyle";
import { scenarioStyle as _scenarioStyle } from "../../public/styles/scenarioStyle";
import {
  StyleSpecification,
  GeoJSONSourceSpecification,
  CircleLayerSpecification,
  SymbolLayerSpecification,
} from "maplibre-gl";
import { scenarios } from "../../public/scenarios";

@Component({
  styleUrls: ["app.css", "../../../node_modules/driver.js/dist/driver.css"],
  tag: "lrtp-app",
})
export class App {
  private surveyCtrl?: HTMLLrtpSurveyControllerElement;
  private map?: HTMLGlMapElement;
  private drawerToggle?: HTMLGlDrawerToggleElement;
  private surveyButton?: HTMLIonButtonElement;
  private commentsStyle: StyleSpecification;
  private scenarioStyle: StyleSpecification;
  private previousIndex: number;
  private commentsSourceId: string;
  private formSchema: string;

  /**
   * The Features API url for managing comment likes.
   */
  private likeUrl: string;

  /**
   * The url to the Features API endpoint to add comments
   */
  private commentUrl: string;

  @Element() el: HTMLLrtpAppElement;

  /**
   * Whether users who view the map are allowed to add new comments or use the survey.
   * This is set to true during Phase 1 when user responses are gathered and set to false later when
   * the public input period has passed.
   */
  @Prop() readonly allowInput: boolean = true;

  /**
   * The bounding box for the map, currently used to bound the address search area
   */
  @Prop() readonly bbox: string;

  /**
   * The endpoint of the geocode api this component connects to.
   */
  @Prop() readonly forwardGeocodeUrl: string;

  /**
   * Whether or not this map is expected to be used by multiple people accessing it from the same device.
   * If set to true the intro will play on each page load and users will be prompted to take the survey multiple times.
   */
  @Prop() readonly multiuser: boolean = false;

  /**
   * Url to the survey (currently google form) the users will be filling out about
   * the Lincoln Avenue Corridor Study.
   */
  @Prop() readonly surveyUrl: string;

  /**
   * The authentication token for the Features API to access comments.
   */
  @Prop() readonly token: string;

  /**
   * The comment that is currently being highlighted by the user.
   */
  @State() selectedComment: Feature<Point>;

  /**
   * The index of the scenario currently being displayed.
   */
  @State() scenarioIndex: number = 0;

  private closeDrawer() {
    let drawer = this.el.querySelector("gl-drawer");
    if (drawer != undefined && drawer.open) drawer.toggle();
  }

  private openDrawer() {
    let drawer = this.el.querySelector("gl-drawer");
    if (drawer != undefined && !drawer.open) drawer.toggle();
  }

  @Listen("body:glFeatureClick")
  handleClick(e: CustomEvent) {
    const features = e.detail.features;
    if (!features || !features.length) return;
    const feature = features[0];
    if (feature.layer.id === "lrtp:comment") {
      this.handleCommentClick();
    } else {
      const zoom = this.map.map.getZoom();
      const maxZoom = this.map.map.getMaxZoom();
      this.map.map.easeTo({
        center: feature.geometry.coordinates,
        zoom: zoom + 1 <= maxZoom ? zoom + 1 : maxZoom,
        duration: 500,
      });
    }
  }

  @Listen("commentSelectedEvent")
  selectComment(e: CustomEvent<Feature<Point>>) {
    this.selectedComment = e.detail;
    this.openDrawer();
  }

  @Listen("scenarioSelected")
  changeScenario(e: CustomEvent<number>) {
    this.scenarioIndex = e.detail;
  }

  private async openSurvey() {
    let alert = await this.surveyCtrl.create();
    return await alert.present();
  }

  private async handleCommentClick() {
    this.closeDrawer();
  }

  private showIntro() {
    let driver: Driver = createDriver({
      animate: false,
      nextBtnText: _t("lrtp.app.intro.next"),
      prevBtnText: _t("lrtp.app.intro.prev"),
      doneBtnText: _t("lrtp.app.intro.done"),
    });

    let steps = [
      {
        element: "gl-map",
        popover: {
          title: _t("lrtp.app.intro.welcome.title"),
          description: `<p>${_t("lrtp.app.intro.welcome.text", {
            app_label: "<strong>" + _t("lrtp.app.label") + "</strong>",
          })}</p><p>(1 / ${this.allowInput ? 7 : 4})</p>`,
        },
      },
      {
        element: "lrtp-comment-detail",
        popover: {
          title: _t("lrtp.app.intro.view.title"),
          description: `<p>${_t("lrtp.app.intro.view.text")}</p><p>(2 / ${
            this.allowInput ? 7 : 4
          })</p>`,
        },
      },
      {
        element: ".lrtp-locate-button",
        popover: {
          title: _t("lrtp.app.intro.locate.title"),
          description: `<p>${_t("lrtp.app.intro.locate.text")}</p><p>(3 / ${
            this.allowInput ? 7 : 4
          })</p>`,
          position: "left",
        },
      },
      {
        element: ".lrtp-scenario-selector",
        popover: {
          title: _t("lrtp.app.intro.scenarios.title"),
          description: `<p>${_t("lrtp.app.intro.scenarios.text")}</p><p>(4 / ${
            this.allowInput ? 7 : 4
          })</p>`,
          position: "below",
        },
      },
    ];

    if (this.allowInput)
      steps.push(
        {
          element: "gl-like-button",
          popover: {
            title: _t("lrtp.app.intro.like.title"),
            description: `<p>${_t(
              "lrtp.app.intro.like.text"
            )}</p><p>(5 / 7)</p>`,
            position: "left",
          },
        },
        {
          element: "ion-fab",
          popover: {
            title: _t("lrtp.app.intro.add.title"),
            description: `<p>${_t(
              "lrtp.app.intro.add.text"
            )}</p><p>(6 / 7)</p>`,
            position: "left",
          },
        },
        {
          element: ".lrtp-survey-button",
          popover: {
            title: _t("lrtp.app.intro.survey.title"),
            description: `<p>${_t(
              "lrtp.app.intro.survey.text"
            )}</p><p>(7 / 7)</p>`,
            position: "left",
          },
        }
      );

    driver.setSteps(steps);
    driver.drive();
  }

  private populateToken(style: StyleSpecification, token: string) {
    if (token && style.sources != undefined) {
      for (let srcName in style.sources) {
        let src = style.sources[srcName] as GeoJSONSourceSpecification;
        if (src.data != undefined)
          src.data = (src.data as string).replace(/\$\{TOKEN\}/g, this.token);
      }
    }
  }

  private updateCommentSources(style: StyleSpecification) {
    this.commentsSourceId = Object.keys(style.sources)[0];

    if (
      this.commentsSourceId != undefined &&
      style.layers != undefined &&
      Array.isArray(style.layers) &&
      style.layers.length != 0
    ) {
      for (let layer of style.layers) {
        (layer as CircleLayerSpecification | SymbolLayerSpecification).source =
          this.commentsSourceId;
      }
    }
  }

  private updateScenario(
    commentStyle: StyleSpecification,
    scenarioStyle: StyleSpecification,
    scenarioIndex: number,
    token: string
  ) {
    if (scenarios == undefined) {
      console.log("No scenarios are defined, cannot load scenario");
      return null;
    } else if (scenarios.length === 0) {
      console.log("List of scenarios is empty, cannot select scenario");
      return null;
    } else if (scenarioIndex < 0 || scenarioIndex >= scenarios.length) {
      console.log(
        `Scenario Index, ${scenarioIndex}, is outside of bounds for list of scenarios length, ${scenarios.length}`
      );
      return null;
    }

    localStorage.setItem("scenarioIndex", this.scenarioIndex.toString());

    const scenario = scenarios[scenarioIndex];
    commentStyle.sources = scenario.commentDataSource.sources;
    scenarioStyle.sources = scenario.scenarioDataSource.sources;
    this.likeUrl = scenario.likeUrl;
    this.commentUrl = scenario.commentUrl;
    this.formSchema = scenario.commentForm;
    this.populateToken(commentStyle, token);
    this.updateCommentSources(commentStyle);
  }

  async componentWillLoad() {
    if (this.multiuser) localStorage.clear();
    this.commentsStyle = { ..._commentsStyle };
    this.scenarioStyle = { ..._scenarioStyle };

    const existingIndex: number = parseInt(
      localStorage.getItem("scenarioIndex")
    );
    if (!Number.isNaN(existingIndex)) {
      this.scenarioIndex = existingIndex;
    }

    this.updateScenario(
      this.commentsStyle,
      this.scenarioStyle,
      this.scenarioIndex,
      this.token
    );
    this.previousIndex = this.scenarioIndex;
  }

  async componentDidLoad() {
    if (this.map.map != undefined) {
      // Here we limit the map to just the lincoln avenue corridor study area, as defined by the extant of G:\CUUATS\Lincoln Avenue Corridor Study\Data\Area_of_Influence.gpkg
      this.map.map.setMaxBounds([
        [-88.27, 40.0955542],
        [-88.17, 40.1138],
      ]);

      // disable map rotation using right click + drag
      this.map.map.dragRotate.disable();

      // disable map rotation using touch rotation gesture
      this.map.map.touchZoomRotate.disableRotation();
    }

    if (this.surveyCtrl == undefined)
      this.surveyCtrl = await findOrCreateOnReady("lrtp-survey-controller");

    let shown = localStorage.getItem("lrtp.intro") === "true";
    if (!shown) {
      localStorage.setItem("lrtp.intro", "true");
      // TODO: Figure out a better way to determine when the feature list
      // is loaded.
      setTimeout(() => this.showIntro(), 1000);
    }
  }

  private clickedDrawerLabel = () => {
    if (this.drawerToggle != undefined)
      (
        this.drawerToggle.querySelector(
          'ion-button, [title="Toggle drawer"]'
        ) as HTMLElement
      ).click();
  };

  private doClickSurvey = () => {
    if (this.surveyButton != undefined) this.surveyButton.click();
  };

  private doOpenSurvey = () => this.openSurvey();

  private doCloseDrawer = () => this.closeDrawer();

  render() {
    if (this.scenarioIndex !== this.previousIndex) {
      this.updateScenario(
        this.commentsStyle,
        this.scenarioStyle,
        this.scenarioIndex,
        this.token
      );
      this.previousIndex = this.scenarioIndex;
    }

    const surveyButton = this.allowInput
      ? [
          <ion-label slot="end-buttons" onClick={this.doClickSurvey}>
            {_t("lrtp.button-labels.survey")}
          </ion-label>,
          <ion-button
            slot="end-buttons"
            class="lrtp-survey-button"
            onClick={this.doOpenSurvey}
            ref={(elm: HTMLIonButtonElement) => {
              this.surveyButton = elm;
            }}
          >
            <ion-icon slot="icon-only" name="bulb"></ion-icon>
          </ion-button>,
        ]
      : null;

    const addButton = this.allowInput ? (
      <ion-fab vertical="bottom" horizontal="end">
        <lrtp-comment-add
          url={this.commentUrl}
          token={this.token}
          onClick={this.doCloseDrawer}
          schema={this.formSchema}
          label={_t("lrtp.app.comment.add")}
          toolbarLabel={_t("lrtp.app.comment.location")}
        ></lrtp-comment-add>
      </ion-fab>
    ) : null;

    return (
      <Host>
        <gl-app label={_t("lrtp.app.label")} menu={false}>
          <gl-fullscreen slot="start-buttons"></gl-fullscreen>
          <gl-basemaps slot="start-buttons"></gl-basemaps>
          {surveyButton}
          <ion-label slot="end-buttons" onClick={this.clickedDrawerLabel}>
            {_t("lrtp.button-labels.drawer")}
          </ion-label>
          <gl-drawer-toggle
            slot="end-buttons"
            icon="chatbubbles"
            ref={(elm: HTMLGlDrawerToggleElement) => {
              this.drawerToggle = elm;
            }}
          ></gl-drawer-toggle>
          <lrtp-scenario-selector
            slot="end-buttons"
            scenarios={scenarios}
            scenarioIndex={this.scenarioIndex}
            class="lrtp-scenario-selector"
          ></lrtp-scenario-selector>
          <gl-address-search
            bbox={this.bbox}
            url={this.forwardGeocodeUrl}
          ></gl-address-search>
          <gl-map
            ref={(r: HTMLGlMapElement) => (this.map = r)}
            longitude={-88.218767}
            latitude={40.105387}
            zoom={13}
            maxzoom={22}
          >
            <gl-style
              json={this.commentsStyle}
              id="lrtp"
              clickableLayers={["cluster", "comment"]}
              name={_t("lrtp.app.comment.label")}
              enabled={true}
              token={this.token}
            >
              <gl-popup
                layers={["comment"]}
                component="lrtp-comment-detail"
                componentOptions={{
                  popup: true,
                }}
              ></gl-popup>
            </gl-style>
            <gl-style
              json={this.scenarioStyle}
              basemap={false}
              name={_t("lrtp.app.scenarios.label")}
              enabled={true}
            ></gl-style>
            <gl-style
              url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/tiles/imagery-2023/13/2087/3098.png"
              name={_t("lrtp.app.basemap.hybrid")}
              enabled={true}
            ></gl-style>
          </gl-map>
          {addButton}
          <gl-drawer
            slot="after-content"
            open={true}
            drawer-title={_t("lrtp.app.comment.label")}
          >
            <gl-feature-list
              styleId="lrtp"
              sourceId={this.commentsSourceId}
              item={false}
              component="lrtp-comment-detail"
              features={[]}
              componentOptions={{
                allowLike: this.allowInput,
                likeUrl: this.likeUrl,
                token: this.token,
                scenarioIndex: this.scenarioIndex,
                selectedId:
                  this.selectedComment != undefined
                    ? this.selectedComment.id
                    : null,
              }}
              orderBy="_created"
              order="desc"
            ></gl-feature-list>
          </gl-drawer>
          <gl-draw-toolbar slot="footer"></gl-draw-toolbar>
        </gl-app>
      </Host>
    );
  }
}
