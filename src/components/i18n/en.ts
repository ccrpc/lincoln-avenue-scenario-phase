export const phrases = {
  lrtp: {
    "address-search": {
      prompt: "Search by address or place name...",
    },
    app: {
      label: "Lincoln Avenue Infrastructure Feedback - Scenarios",
      basemap: {
        imagery: "Imagery",
        hybrid: "Imagery with Labels",
      },
      scenarios: {
        label: "Scenario",
        selection: {
          text: "Select which scenario to view",
        },
        scenario_1: {
          label: "Scenario One",
          description: "Continuous Bike Lanes",
        },
        scenario_2: {
          label: "Scenario Two",
          description: "Shared Use path between bike lanes",
        },
        scenario_3: {
          label: "Scenario Three",
          description: "North Bike lane, South Shared Path",
        },
      },
      comment: {
        label: "Comments",
        location: "Choose a Location",
        add: "Add a Comment",
        added: "Your comment has been added.",
        moderation: "Your comment is awaiting moderation.",
        error: "An error occurred. Please try again later.",
      },
      intro: {
        next: "Next",
        prev: "Prev",
        done: "Done",
        close: "Close",
        welcome: {
          title: "Welcome",
          text: "Welcome to %{app_label} by the Champaign County Regional Planning Commission. You can browse the map to see comments.",
        },
        scenarios: {
          title: "Select Scenario",
          text: "Select which scenario to view.",
        },
        view: {
          title: "View Comments",
          text: "The comments pane shows the details of all comments currently visible in the map.",
        },
        locate: {
          title: "Locate a Comment",
          text: "Tap the locate button to zoom the map to a comment.",
        },
        like: {
          title: "Like a Comment",
          text: "Show your support for a comment by tapping the star.",
        },
        add: {
          title: "Add a Comment",
          text: "Add a comment by tapping the plus button, selecting a location, and filling out the comment form.",
        },
        survey: {
          title: "Take the Survey",
          text: "Tap the lightbulb to take a brief survey about transportation infrastructure along the Lincoln Avenue Corridor. Happy browsing!",
        },
      },
    },
    "button-labels": {
      survey: "Take the Survey",
      drawer: "Comments",
    },
    "survey-controller": {
      title: "Take the Transportation Survey",
      text: "Please share your thoughts about the transportation infrastructure along the Lincoln Avenue Corridor by taking a brief survey. You can access the survey later using the %{icon} icon in the top toolbar.",
      later: "Take Later",
      now: "Take Now",
    },
    form: {
      labels: {
        mode: "Choose a Mode of Transportation",
        improvement: "Choose what Improvement to comment on",
        category: "Choose a Category",
        comment: "Choose a Comment",
        details: "Provide Additional Details (optional)",
      },
      improvement: {
        label: "Improvement Feedback",
        consistent: {
          label: "Suggested Improvements that apply to every Scenario",
          realign: {
            label:
              "Re-align Lincoln Avenue roadway to 3-lanes (left turn lane at signalized intersections and no right turn lanes)",
            accept: "Implement -- Re-align Lincoln Avenue roadway",
            modify: "Implement Modified -- Re-align Lincoln Avenue roadway",
            reject: "Do Not Implement -- Re-align Lincoln Avenue roadway",
          },
          implement: {
            label:
              "Implement right-turn-only lanes entering the corridor at Green and Florida",
            accept: "Implement -- Right-turn-only lanes",
            modify: "Implement modified -- Right-turn-only lanes",
            reject: "Do Not Implement -- Right-turn-only lanes",
          },
          adjust: {
            label:
              "Adjust signal timing along the corridor to accommodate smooth traffic flow with other changes",
            accept: "Implement -- Adjust signal timing",
            modify: "Implement Modified -- Adjust signal timing",
            reject: "Do Not Implement -- Adjust signal timing",
          },
          limit: {
            closing: {
              label:
                "Limit access to side streets by closing off vehicle access at Lincoln Avenue",
              accept:
                "Implement -- Limit access to side streets: closing off vehicle access",
              modify:
                "Implement Modified -- Limit access to side streets: closing off vehicle access",
              reject:
                "Do Not Implement -- Limit access to side streets: closing off vehicle access",
            },
            order: {
              label:
                "Limit access to side streets by implementing Right In/Right Out",
              accept:
                "Implement -- Limit access to side streets: Right In/Right Out",
              modify:
                "Implement Modified -- limit access to side streets: Right In/Right Out",
              reject:
                "Do Not Implement -- limit access to side streets: Right In/Right Out",
            },
          },
          relocate: {
            label: "Relocate or consolidate crosswalks",
            accept: "Implement -- Relocate or consolidate crosswalks",
            modify: "Implement Modified -- Relocate or consolidate crosswalks",
            reject: "Do Not Implement -- Relocate or consolidate crosswalks",
          },

          beacons: {
            label:
              "Install rectangular rapid flashing beacons (RRFB) at crosswalks",
            accept: "Implement -- Install rectangular rapid flashing beacons",
            modify:
              "Implement Modified -- Install rectangular rapid flashing beacons",
            reject:
              "Do Not Implement -- Install rectangular rapid flashing beacons",
          },
        },
        scenarios: {
          label: "Scenario Specific Improvement",
          one: {
            install: {
              label:
                "Install continuous on-street bike lanes in both directions from Green to Florida",
              accept: "Implement -- Install Bike Lanes",
              modify: "Implement Modified -- Install Bike Lanes",
              reject: "Do Not Implement -- Install Bike Lanes",
            },
          },
          two: {
            install_green: {
              label:
                "Implement -- Install on-street bike lanes in both directions between Green and Iowa",
              accept:
                "Implement -- Install on-street bike lanes between Green and Iowa",
              modify:
                "Implement Modified -- Install on-street bike lanes between Green and Iowa",
              reject:
                "Do Not Implement -- Install on-street bike lanes between Green and Iowa",
            },
            install_pennsylvania: {
              label:
                "Install on-street bike lanes in both directions between Pennsylvania and Florida",
              accept:
                "Implement -- Install on-street bike lanes - Pennsylvania and Florida",
              modify:
                "Implement Modified -- Install on-street bike lanes - Pennsylvania and Florida",
              reject:
                "Do Not Implement -- Install on-street bike lanes - Pennsylvania and Florida",
            },
            construct: {
              label:
                "Construct shared-use path on west side of street from Iowa to Pennsylvania",
              accept:
                "Implement -- Construct shared-use path: Iowa to Pennsylvania",
              modify:
                "Implement Modified -- Construct shared-use path: Iowa to Pennsylvania",
              reject:
                "Do Not Implement -- Construct shared-use path: Iowa to Pennsylvania",
            },
          },
          three: {
            install: {
              label:
                "Install on-street bike lanes in both directions between Green and Iowa",
              accept: "Implement -- Install On-Street Bike Lanes",
              modify: "Implement Modified -- Install On-Street Bike Lanes",
              reject: "Do Not Implement -- Install On-Street Bike Lanes",
            },
            construct: {
              label:
                "Construct shared-use path on west side of street from Iowa to Florida",
              accept: "Implement -- Construct Shared-Use Path: Iowa to Florida",
              modify:
                "Implement Modified -- Construct Shared-Use Path: Iowa to Florida",
              reject:
                "Do Not Implement -- Construct Shared-Use Path: Iowa to Florida",
            },
          },
        },
        response: {
          accept: "This intervention is appropriate at this location",
          modify: "Implement, but modify, this intervention here",
          reject: "This intervention is inappropriate at this location",
        },
      },

      pedestrian: {
        label: "Walking or Wheelchair",
        crosswalk: {
          label: "Crosswalks",
          ok: "Crosswalk functions well",
          add: "Add a crosswalk here",
          pavement: "Fix uneven pavement in crosswalk",
          repaint: "Repaint crosswalk",
          median: "Add pedestrian refuge/median",
          distance: "Reduce crossing distance",
        },
        curbramp: {
          label: "Curb Ramps",
          ok: "Curb ramp functions well",
          add: "Add a curb ramp here",
          design: "Change curb ramp design (please describe below)",
          repair: "Repair or replace curb ramp",
        },
        sidewalk: {
          label: "Sidewalks",
          ok: "Sidewalk functions well",
          add: "Add a sidewalk here",
          obstruction: "Remove a sidewalk obstruction (please describe below)",
          repair: "Repair or replace sidewalk",
          widen: "Widen narrow sidewalk",
        },
        sign: {
          label: "Signs and Signals",
          ok: "Signs and signals function well",
          stop: "Add stop signs here",
          pedestriansignal: "Add pedestrian signals here",
          trafficsignal: "Add traffic signals here",
          crossingtime: "Increase crossing time",
          movesignal: "Move pedestrian signal or pushbutton",
          speedlimit: "Reduce roadway speed limit",
        },
        other: {
          label: "Other",
          goodaccessibility: "Good accessibility for multiple modes here",
          lighting: "Add lighting here",
          roundabout: "Convert intersection to a roundabout",
          separation: "Increase separation between motorists and pedestrians",
          other: "Other (please describe below)",
        },
      },
      bicycle: {
        label: "Bicycle",
        facility: {
          label: "Bike Lanes and Paths",
          ok: "Bike lanes and paths function well",
          onstreet: "Add an on-street bike lane",
          offstreet: "Add an off-street sidepath or trail",
          pavement: "Fix uneven pavement in bike lane or path",
          repair: "Repair or replace bike lanes or paths",
          barriers: "Add physical barriers to on-street bike lane",
        },
        parking: {
          label: "Bike Parking",
          ok: "Bike parking functions well",
          add: "Add bike racks",
          change: "Change type of bike racks",
          repair: "Repair or replace bike racks",
        },
        sign: {
          label: "Signs and Signals",
          ok: "Signs and signals function well",
          signal: "Add bike-activated traffic signals here",
          stop: "Add stop signs here",
          wayfinding: "Add or improve wayfinding signs for cyclists",
          crossingtime: "Increase crossing time",
          speedlimit: "Reduce speed limit",
        },
        other: {
          label: "Other",
          goodaccessibility: "Good accessibility for multiple modes here",
          lighting: "Add lighting here",
          roundabout: "Convert intersection to a roundabout",
          visibility: "Improve visibility for cyclists",
          separation: "Increase separation between motorists and cyclists",
          other: "Other (please describe below)",
        },
      },
      bus: {
        label: "Bus",
        amenity: {
          label: "Benches, Shelters, and Amenities",
          ok: "Amenities at this stop function well",
          bench: "Add a bench at this stop",
          landingpad: "Add a landing pad at the stop for easier boarding",
          shelter: "Add a shelter at this stop",
          audiblesign:
            "Add audible signage at this stop for routes and information",
          bikeparking: "Add bike parking at this stop",
          lighting: "Add lighting at this stop",
        },
        stop: {
          label: "Bus Stop Locations",
          ok: "Bus stop location functions well",
          safe: "I feel safe at this stop",
          unsafe: "I feel unsafe at this stop",
          add: "Add a stop here",
          move: "Move this stop (please describe below)",
          remove: "Remove this stop",
        },
        schedule: {
          label: "Schedule and Service",
          ok: "Bus service at this location functions well",
          add: "Add a new route here (please describe below)",
          frequency: "Increase bus frequency here",
          weekday: "Increase weekday service hours here",
          weekend: "Increase weekend service hours here",
          delays: "Bus: Reduce bus delays here",
        },
        other: {
          label: "Other",
          goodaccessibility: "Good accessibility for multiple modes here",
          pavement: "Improve pavement condition for buses",
          separation:
            "Increase separation between buses and other roadway users",
          conflicts: "Reduce conflicts between buses and other roadway users",
          other: "Other (please describe below)",
        },
      },
      automobile: {
        label: "Automobile",
        driving: {
          label: "Driving Conditions",
          ok: "Good automobile circulation here",
          lighting: "Add street lighting",
          design: "Change roadway design or width (please describe below)",
          pavement: "Fix uneven pavement",
          markings: "Improve pavement markings",
          visibility: "Improve visibility for drivers",
          speedlimit: "Reduce speed limit",
          congestion: "Reduce traffic congestion",
        },
        multimodal: {
          label: "Pedestrian, Bike, and Bus Facilities",
          ok: "Good accessibility for multiple modes here",
          bikelane: "Add an on-street bike lane",
          sidepath: "Add an off-street sidepath",
          sidewalk: "Add an off-street sidewalk",
          buslane: "Add a dedicated bus lane",
        },
        sign: {
          label: "Signs and Signals",
          ok: "Signs and signals function well",
          signal: "Improve signal sensor or timing here",
          stop: "Add stop signs here",
          wayfinding: "Add or improve wayfinding signs for drivers",
          crossing: "Increase crossing time",
          speedlimit: "Reduce speed limit",
        },
        parking: {
          label: "Parking",
          ok: "Parking functions well",
          add: "Add automobile parking here",
          reduce: "Reduce automobile parking here",
        },
        other: {
          label: "Other",
          goodaccessibility: "Good accessibility for multiple modes here",
          roundabout: "Convert intersection to a roundabout",
          visibility: "Improve visibility for drivers",
          separation: "Increase separation between motorists and cyclists",
          other: "Other (please describe below)",
        },
      },
    },
  },
};
