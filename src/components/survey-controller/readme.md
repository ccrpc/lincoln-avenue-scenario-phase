# lrtp-survey-controller



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description                                                                                                                                                                                                                       | Type     | Default |
| ------------------ | ------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------- |
| `commentThreshold` | `comment-threshold` | . How many comments the user makes before they are prompted them to take the survey.  If the user submits comments >= the threshold OR likes a number of comments >= the like threshold, they will be prompted to take the survey | `number` | `1`     |
| `delay`            | `delay`             | How long, in milliseconds, after the user has surpassed either threshold before they are prompted to take the survey. If the user exceeds both thresholds within this time they will still only be prompted once.                 | `number` | `3000`  |
| `likeThreshold`    | `like-threshold`    | How many likes the users leaves before they are prompted to take the survey.  If the user submits comments >= the threshold OR likes a number of comments >= the like threshold, they will be prompted to take the survey         | `number` | `2`     |


## Methods

### `create() => Promise<HTMLIonAlertElement>`

Creates the ionic alert controller which will open the alert which prompts the user to take the survey

#### Returns

Type: `Promise<HTMLIonAlertElement>`

A promise to the ionic alert controller which will handle the survey prompt


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
