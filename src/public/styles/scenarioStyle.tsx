import { StyleSpecification } from "@ccrpc/webmapgl";

export const scenarioStyle: StyleSpecification = {
  version: 8,
  glyphs: "https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf",
  sources: {
    // Populated by the scenarios.tsx file.
  },
  layers: [
    {
      id: "scenario",
      type: "raster",
      source: "scenario",
    },
  ],
};
