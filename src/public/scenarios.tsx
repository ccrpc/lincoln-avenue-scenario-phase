import { SourceSpecification } from "maplibre-gl";

export interface scenarioInterface {
  label: string;
  description: string;
  commentUrl: string;
  likeUrl: string;
  commentForm: string;
  commentDataSource: {
    sources: { [key: string]: SourceSpecification };
  };
  scenarioDataSource: {
    sources: {
      scenario: SourceSpecification;
    };
  };
}

export const scenarios: scenarioInterface[] = [
  {
    label: "lrtp.app.scenarios.scenario_1.label",
    description: "lrtp.app.scenarios.scenario_1.description",
    commentUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_one_comment",
    likeUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_one_comment_like",
    commentForm:
      "/lincoln-avenue-scenario-phase/public/comment-forms/scenario-one-form.json",
    commentDataSource: {
      sources: {
        scenarioOneComments: {
          type: "geojson",
          data: "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_one_comment_web?token=${TOKEN}",
          cluster: true,
          clusterMaxZoom: 14,
          clusterRadius: 50,
        },
      },
    },
    scenarioDataSource: {
      sources: {
        scenario: {
          type: "raster",
          url: "https://maps.ccrpc.org/lincoln-avenue-scenario-tiles/lincoln-avenue-scenario-1.json",
        },
      },
    },
  },
  {
    label: "lrtp.app.scenarios.scenario_2.label",
    description: "lrtp.app.scenarios.scenario_2.description",
    commentUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_two_comment",
    likeUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_two_comment_like",
    commentForm:
      "/lincoln-avenue-scenario-phase/public/comment-forms/scenario-two-form.json",
    commentDataSource: {
      sources: {
        scenarioTwoComments: {
          type: "geojson",
          data: "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_two_comment_web?token=${TOKEN}",
          cluster: true,
          clusterMaxZoom: 14,
          clusterRadius: 50,
        },
      },
    },
    scenarioDataSource: {
      sources: {
        scenario: {
          type: "raster",
          url: "https://maps.ccrpc.org/lincoln-avenue-scenario-tiles/lincoln-avenue-scenario-2.json",
        },
      },
    },
  },
  {
    label: "lrtp.app.scenarios.scenario_3.label",
    description: "lrtp.app.scenarios.scenario_3.description",
    commentUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_three_comment",
    likeUrl:
      "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_three_comment_like",
    commentForm:
      "/lincoln-avenue-scenario-phase/public/comment-forms/scenario-three-form.json",
    commentDataSource: {
      sources: {
        scenarioThreeComments: {
          type: "geojson",
          data: "https://features.ccrpc.org/public/lincoln_avenue_scenarios.scenario_three_comment_web?token=${TOKEN}",
          cluster: true,
          clusterMaxZoom: 14,
          clusterRadius: 50,
        },
      },
    },
    scenarioDataSource: {
      sources: {
        scenario: {
          type: "raster",
          url: "https://maps.ccrpc.org/lincoln-avenue-scenario-tiles/lincoln-avenue-scenario-3.json",
        },
      },
    },
  },
];
